<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 27/01/18
 * Time: 22:12
*/
session_start();
$uncompleted=False; // Used as a global var to know if we have to redirect to index.php or not.

// Construction of error messages.
$_SESSION['nom']['msg'] = "Veuillez remplir le nom.</br>";
$_SESSION['prenom']['msg'] = "Veuillez remplir le prénom.</br>";
$_SESSION['email']['msg'] = "Veuillez remplir l'email.</br>";
$_SESSION['mdp']['msg'] = "Veuillez choisir un mot de passe</br>";
$_SESSION['mdp_confirmer']['msg'] = "La confirmation du mot de passe n'est pas juste.</br>";

// If there is no value for a $key we say it's missing and we redirect.
// If it's not missing we store the value in $_SESSION.
foreach ($_POST as $key => $value){
    $_SESSION[$key]['missing']=0;
    $_SESSION[$key]['value']=null;
    if ($value == ''){
        $uncompleted = True;
        $_SESSION[$key]['missing'] = 1;
    }
    else{
        $_SESSION[$key]['value'] = $value;
    }
}

$_SESSION['mdp']['value'] != $_SESSION['mdp_confirmer']['value']?$uncompleted=True & $_SESSION['mdp']['missing'] = 1:'';

// Check if there a password was entered.
//?$uncompleted = True & $_SESSION['mdp_confirmer']['missing'] = 0:$uncompleted = False;
// Check if the password and the confirmation match.
//if (empty($_SESSION['mdp']['value'])){
//    $uncompleted = True;
//    $_SESSION['missing'] = 1;
//}
//elseif ($_SESSION['mdp'] != $_SESSION['mdp_confirmer']){
//    $uncompleted = True;
//    $_SESSION['missing'] = 1;
//}
//else{
//    $uncompleted = False;
//    $_SESSION['missing'] = 0;
//}

$uncompleted?header('location: index.php'):'';

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
        .card {
            /* Add shadows to create the "card" effect */
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
            width: 200px;
            margin: auto;
        }

        /* On mouse-over, add a deeper shadow */
        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
        }

        /* Add some padding inside the card container */
        .container {
            padding: 2px 16px;
        }
    </style>
    <title>Belle carte</title>
</head>
<body>

<div class="card">
    <img src="./images/img.png" alt="Avatar" style="width:100%">
    <div class="container">
        <h4><b><?php echo $_POST['nom'] . " " . $_POST['prenom'] ?></b></h4>
        <p><?php echo $_POST['email'] ?></p>
    </div>
</div>
</body>
</html>