<?php
session_start();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/>
    <title>Beau formulaire</title>
</head>
<body>
<!--Hello, use me to reset this session !-->
<?php if (isset($_GET['debug'])) {
        echo '  <form action="reset.php" method="post">
                <input type="submit" value="RESET!"/>
                </form>';
        }
?>
<!--Error Message !-->
<?php
foreach($_SESSION as $key => $v){
    echo $_SESSION[$key]['missing'] ==1?'<div class="alert alert-danger">' . $_SESSION[$key]['msg'] . '</div>':'';
}
?>
    <div style="margin: auto;width: 300px;">
        <form action="recup.php" method="post" class="form-group">
            <legend><b>Entrez vos coordonnées</b></legend>
            <label>Nom: </label>
            <input type="texte" name="nom" class="form-control
                <?php echo $_SESSION['nom']['missing'] == 1?"is-invalid": "" ?>"
                value="<?php echo $_SESSION['nom']['value']??null; ?>"
            /><br/>
            <label>Prénom: </label>
            <input type="texte" name="prenom" class="form-control
                <?php echo $_SESSION['prenom']['missing'] == 1?"is-invalid": "" ?>"
                value="<?php echo $_SESSION['prenom']['value']??null; ?>"
            /><br/>
            <label>E-mail: </label>
            <input type="email" name="email" class="form-control
                <?php echo $_SESSION['email']['missing'] == 1?"is-invalid": "" ?>"
                value="<?php echo $_SESSION['email']['value']??null; ?>"
            /><br/>
            <label>Mot de passe: </label>
            <input type="password" name="mdp" class="form-control"/><br/>
            <label>Confirmer le mot de passe: </label>
            <input type="password" name="mdp_confirmer" class="form-control"/><br/>

            <input type="submit" class="btn btn-success" class="btn btn-primary"/>
            <input type="reset" class="btn btn-primary" class="btn btn-primary"/>&nbsp;&nbsp;&nbsp;
        </form>
    </div>

</body>
</html>